# from unittest import TestCase
# from src.grid import Grid, ALIVE, DEAD, EMPTY

# SURVIVOR = ALIVE


# class TestGrid(TestCase):
#     def test_grid_WIHOUT_alive_cells_SHOULD_have_no_live_cells(self):
#         grid = Grid([[DEAD, EMPTY], [DEAD, EMPTY]])
#         self.assertFalse(grid.has_live_cells())

#     def test_grid_WITH_at_least_one_alive_cell_SHOULD_have_life_cells(self):
#         grid = Grid([[ALIVE, EMPTY], [DEAD, EMPTY]])
#         self.assertTrue(grid.has_live_cells())

#     def test_live_cell_WITH_less_than_two_neightbors_SHOULD_die(self):
#         grid = Grid([[ALIVE, ALIVE], [DEAD, EMPTY]])
#         self.assertFalse(grid.should_survive(1, 1))

#     def test_live_cell_WITH_two_neightbors_SHOULD_survive(self):
#         grid = Grid([[SURVIVOR, ALIVE], [ALIVE, DEAD]])
#         self.assertFalse(grid.should_survive(0, 0))

#         grid = Grid([[ALIVE, DEAD], [SURVIVOR, ALIVE]])
#         self.assertFalse(grid.should_survive(1, 0))

#         grid = Grid([[ALIVE, SURVIVOR], [DEAD, ALIVE]])
#         self.assertFalse(grid.should_survive(0, 1))

#         grid = Grid([[DEAD, ALIVE], [ALIVE, SURVIVOR]])
#         self.assertFalse(grid.should_survive(1, 1))

#     def test_live_cell_WITH_three_neightbors_SHOULD_survive(self):
#         grid = Grid([[ALIVE, SURVIVOR, ALIVE], [DEAD, ALIVE, DEAD], [DEAD, DEAD, DEAD]])
#         self.assertFalse(grid.should_survive(0, 1))

#     def test_live_cell_WITH_more_than_three_neighbors_SHOULD_die(self):
#         grid = Grid([[DEAD, ALIVE, DEAD], [DEAD, ALIVE, DEAD], [DEAD, DEAD, DEAD]])
#         self.assertFalse(grid.should_survive(0, 1))
