from unittest import TestCase
from unittest.mock import create_autospec, MagicMock
from src.game_of_life import GameOfLife
from src.grid_analyzer import GridAnalyzer
from src.grid import ALIVE, DEAD, EMPTY

ANY_GRID = [[ALIVE, DEAD, EMPTY], [DEAD, EMPTY, EMPTY], [EMPTY, DEAD, DEAD]]


class TestGameOfLife(TestCase):
    def setUp(self):
        self.grid_analyzer = create_autospec(GridAnalyzer)
        self.game = GameOfLife(self.grid_analyzer)

    def test_gameOfLife_start_SHOULD_initialize_grid_analyser(self):
        self.game.start(ANY_GRID)
        self.grid_analyzer.initialize.assert_called_with(ANY_GRID)

    def test_starting_game_WITH_no_live_cells_SHOULD_last_one_generation(self):
        self.game.start(ANY_GRID)
        self.assertEqual(self.game.number_of_generations(), 1)

    def test_game_SHOULD_last_as_long_as_there_are_living_cells(self):
        EXPECTED_NUMBER_OF_TICKS = 2
        self.grid_analyzer.has_living_cells.side_effect = EXPECTED_NUMBER_OF_TICKS * [
            True
        ] + [False]
        print("------------------------")
        print(self.grid_analyzer)
        self.game.start(ANY_GRID)
        print("-----------------ddddddd")
        # self.assertEqual(
        #     self.game.number_of_generations(), EXPECTED_NUMBER_OF_TICKS + 1
        # )

