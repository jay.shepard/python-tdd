from unittest import TestCase
from src.fizzbuzz import Fizzbuzz


class TestFizzBuzz(TestCase):
    def setUp(self):
        self.fizzbuzz = Fizzbuzz()

    def test_fizzbuzz_count_zero_SHOULD_return_empty(self):
        result = self.fizzbuzz.count(0)
        self.assertEqual(result, [])

    def test_fizzbuzz_count_to_2_SHOULD_return_all_numbers(self):
        result = self.fizzbuzz.count(2)
        self.assertEqual(result, [1, 2])

    def test_fizzbuzz_count_to_3_SHOULD_return_a_fizz(self):
        self.validate_fizzbuzz_value_at(index=3, value="fizz")

    def test_fizzbuzz_count_to_5_SHOULD_return_a_buzz(self):
        self.validate_fizzbuzz_value_at(index=5, value="buzz")

    def test_fizzbuzz_count_to_15_SHOULD_return_a_fizzbuzz(self):
        self.validate_fizzbuzz_value_at(index=15, value="fizzbuzz")

    def validate_fizzbuzz_value_at(self, index, value):
        result = self.fizzbuzz.count(index)
        self.assertEqual(result[index - 1], value)

