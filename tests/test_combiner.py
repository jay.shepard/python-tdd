from unittest import TestCase
from src.combiner import Combiner


class CombinerTest(TestCase):
    def setUp(self):
        self.combiner = Combiner()

    def test_squash_empty_array_SHOULD_return_same_array(self):
        self.verify_that_squash_results_in([0, 0, 0, 0], [0, 0, 0, 0])

    def test_squash_single_edge_left_SHOULD_return_same(self):
        self.verify_that_squash_results_in([2, 0, 0, 0], [2, 0, 0, 0])

    def test_squash_without_neighbour_SHOULD_move_to_left_side(self):
        self.verify_that_squash_results_in([0, 2, 0, 0], [2, 0, 0, 0])

    def test_squash_alone_far_right_SHOULD_more_to_left_side(self):
        self.verify_that_squash_results_in([0, 0, 0, 2], [2, 0, 0, 0])

    def test_squash_two_different_SHOULD_not_combine(self):
        self.verify_that_squash_results_in([2, 4, 0, 0], [2, 4, 0, 0])

    def test_squash_two_different_seperate_SHOULD_shift_not_combine(self):
        self.verify_that_squash_results_in([2, 0, 4, 0], [2, 4, 0, 0])

    # def test_squash_single_edge_right_SHOULD_return_same(self):
    #     self.verify_that_squash_results_in([0, 0, 0, 2], [0, 0, 0, 2])

    # def test_squash_all_different_SHOULD_return_same(self):
    #     self.verify_that_squash_results_in([2, 4, 8, 16], [2, 4, 8, 16])

    def verify_that_squash_results_in(self, from_line, to_line):
        result = self.combiner.squash(from_line)
        self.assertListEqual(result, to_line)
