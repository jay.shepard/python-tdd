class Combiner:
    def squash(self, line):
        i = 0
        last_index = len(line) - 1
        while i < last_index and not self.is_rest_of_array_empty(line, i):
            if line[i] == 0:
                self.shift_from_index(line, i)
            else:
                i += 1
        return line

    def is_rest_of_array_empty(self, arr, index):
        for a in arr[index:]:
            if a != 0:
                return False
        return True

    def shift_from_index(self, line, from_index):
        for i in range(from_index, len(line) - 1):
            line[i] = line[i + 1]
            line[i + 1] = 0

