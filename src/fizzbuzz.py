class Fizzbuzz:
    def count(self, to):
        lst = [self.fizz_buzzify(x) for x in range(1, to + 1)]
        return lst

    def fizz_buzzify(self, x):
        if x % 15 == 0:
            return "fizzbuzz"
        elif x % 5 == 0:
            return "buzz"
        elif x % 3 == 0:
            return "fizz"
        return x
