EMPTY = 0
DEAD = 1
ALIVE = 2


class Grid:
    def __init__(self, grid):
        self.grid = grid

    def has_live_cells(self):
        for row in range(len(self.grid)):
            for col in range(len(self.grid[row])):
                if self.grid[row][col] == ALIVE:
                    return True
        return False

    def should_survive(self, row, col):
        neighbours = self.count_alive_neighbours(row, col)
        if neighbours < 2:
            return False
        return True

    # def _change_live_status(self, row, col):
    #     neighbours = self.count_alive_neighbours(row, col)
    #     if neighbours != 2:
    #         self.grid[row][col] = DEAD

    def count_alive_neighbours(self, row, col):
        total = 0
        if row > 0 and self.grid[row - 1][col] == ALIVE:
            total += 1
        if row < len(self.grid) - 1 and self.grid[row + 1][col] == ALIVE:
            total += 1

        if col > 0 and self.grid[row][col - 1] == ALIVE:
            total += 1
        if col < len(self.grid[0]) - 1 and self.grid[row][col + 1] == ALIVE:
            total += 1
        return total
